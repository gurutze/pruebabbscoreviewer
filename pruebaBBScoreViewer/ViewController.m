//
//  ViewController.m
//  pruebaBBScoreViewer
//
//  Created by Jose Cruz Perez Pi on 7/2/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//

#import "ViewController.h"
#import "Corrector.hpp"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:@"prueba.xml"];

    Blackbinder::Corrector* corrector = NULL;
    try{
        corrector = new Blackbinder::Corrector(std::string([documentsPath
                                                            UTF8String]));
        bbscoreviewer = [[BBScoreViewerViewController alloc] initWithScore:documentsPath andCustomization: @"" andOrigin: 2];
    }catch (const exception& e) {
        NSString* exceptionMsg = [NSString stringWithFormat:@"%s", e.what()];
    }
}

- (void) viewDidAppear:(BOOL)animated{
    [bbscoreviewer setDelegate:self];
    [self presentViewController:bbscoreviewer animated:NO completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didFinishWithError:(NSError *)exitError{
    
}

- (void)didFinishWithChangesInScore:(BOOL) hasChangedScore andCustomization:(BOOL) hasChangedCustomization{
}
    
- (void)didReceiveMonitoringEvent: (NSDictionary *) monitoringEvent{
    
}
@end
