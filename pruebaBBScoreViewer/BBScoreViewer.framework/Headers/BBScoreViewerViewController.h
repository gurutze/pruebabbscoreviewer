//
//  BBScoreViewerViewController.h
//  BBScoreViewer
//
//  Created by Jose Cruz Perez Pi on 17/1/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BBScoreViewerDelegate;

@interface BBScoreViewerViewController : UINavigationController

@property (nonatomic, assign) id <BBScoreViewerDelegate, UINavigationControllerDelegate> delegate;

-(id) initWithScore: (NSString *) initScore andCustomization: (NSString *) initCustomization andOrigin: (int) initOrigin;

@end

@protocol BBScoreViewerDelegate

- (void)didFinishWithError:(NSError *)exitError;
- (void)didFinishWithChangesInScore:(BOOL)hasChangedScore andCustomization:(BOOL) hasChangedCustomization;
- (void)didReceiveMonitoringEvent:(NSDictionary *) monitoringEvent;

@end
