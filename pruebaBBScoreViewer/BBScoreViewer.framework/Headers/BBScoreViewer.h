//
//  BBScoreViewer.h
//  BBScoreViewer
//
//  Created by Jose Cruz Perez Pi on 17/1/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//
#import <UIKit/UIKit.h>
//! Project version number for BBScoreViewer.
FOUNDATION_EXPORT double BBScoreViewerVersionNumber;

//! Project version string for BBScoreViewer.
FOUNDATION_EXPORT const unsigned char BBScoreViewerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BBScoreViewer/PublicHeader.h>
#import <BBScoreViewer/BBScoreViewerViewController.h>
#import <BBScoreViewer/Constants.h>
