#pragma once

#include <string>
#include <vector>

using namespace std;
namespace Blackbinder {

class Loader;
class Corrector {
 public:
  explicit Corrector(const string &filePath);
  ~Corrector();
  bool saveXml(string filePath);

  int getZoom();
  string getTitulo();
  int getNumSubtitulos();
  string getSubtitulo(int numSubtitulo);
  vector<string> getSubtitulos();
  string getInstrumento();
  string getMovimiento();
  string getCompositor();
  string getArreglista();
  string getLetrista();
  float getAlturaPentagrama();
  string getDedicatoria();

  void setZoom(int zoom);
  void setTitulo(string titulo);
  void setSubtitulos(vector<string> subtitulo);
  void setInstrumento(string instrumento);
  void setMovimiento(string movimiento);
  void setCompositor(string compositor);
  void setArreglista(string arreglista);
  void setLetrista(string letrista);
  void setAlturaPentagrama(float altura);
  void setDedicatoria(string dedicatoria);

 private:
  unique_ptr<Loader> loader;
};

}
