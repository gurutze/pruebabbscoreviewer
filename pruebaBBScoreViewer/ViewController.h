//
//  ViewController.h
//  pruebaBBScoreViewer
//
//  Created by Jose Cruz Perez Pi on 7/2/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BBScoreViewer/BBScoreViewerViewController.h>

@interface ViewController : UIViewController <BBScoreViewerDelegate, UINavigationControllerDelegate>{

    BBScoreViewerViewController *bbscoreviewer;
}


@end

